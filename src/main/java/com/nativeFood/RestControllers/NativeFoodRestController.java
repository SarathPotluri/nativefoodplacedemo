package com.nativeFood.RestControllers;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nativeFood.documents.PlaceDocument;
import com.nativeFood.services.NativeFoodService;

@RestController
public class NativeFoodRestController {
	@Autowired
	private NativeFoodService nsi;
	@GetMapping("/places")
	public List<PlaceDocument> getlist()
	{
		
		System.out.println("Get  All places List");
		return nsi.getlist();
	}
	
	
    @RequestMapping(value="/createPlace", method = RequestMethod.POST)

	public PlaceDocument createPlace(@Valid @RequestBody PlaceDocument pd)
	{
    	 pd.setPlaceId(UUID.randomUUID());
    	System.out.println("Place Data Inserted Successfully");
		return nsi.createPlace(pd);
	}
	
   
    @GetMapping("/placeF/{placeId}")
    public PlaceDocument getOnePlace(@PathVariable String placeId)
    {
    	UUID data=UUID.fromString(placeId);
    	System.out.println("String UUId"+data);
    	return nsi.getPlaceByPlaceId(data);
    	
    }

	
	
	  @RequestMapping(value="/place/{placeId}",method = RequestMethod.PUT)  
	    public String update(@PathVariable String placeId, @RequestBody PlaceDocument pd) {
		  UUID data=UUID.fromString(placeId);
          	  nsi.update(data, pd);
	        return "Data Updated Successfully";
	    }
	   @RequestMapping(value = "placeD/{placeId}", method = RequestMethod.DELETE) 
	    public String delete(@PathVariable String placeId) {
		   UUID data=UUID.fromString(placeId);
		   System.out.println("Data Deleted Successfully");
	       return nsi.delete(data);
	        
	    }
	   @DeleteMapping("/place/deleteAll")
	   public String deleteAll()
	   {
		  return nsi.deleteTotal();
		   
	   }
	 /* @RequestMapping(value="/place/{place}",method = RequestMethod.PUT)  
	    public String update(@PathVariable String place, @RequestBody PlaceDocument pd) {
            	  nsi.update(place, pd);
	        return "Data Updated Successfully";
	    }
	   @RequestMapping(value = "placeD/{id}", method = RequestMethod.DELETE) 
	    public String delete(@PathVariable String id) {
		   
		   System.out.println("Data Deleted Successfully");
	       nsi.delete(id);
	        return "Data Deleted Successfully";
	    }*/
}