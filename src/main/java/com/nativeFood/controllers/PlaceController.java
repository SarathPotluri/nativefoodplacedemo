package com.nativeFood.controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nativeFood.documents.PlaceDocument;
import com.nativeFood.repositories.PlaceRepository;

@Controller
public class PlaceController {
	PlaceDocument pd;
	@Autowired
	PlaceRepository pr;
	@RequestMapping("/place")
	public String placeView(Model m)
	{
	m.addAttribute("place", pd);
	return "placeViews/place";
	}
	
	@RequestMapping(value="/addPlace",method=RequestMethod.POST)
	public String addPlace(@ModelAttribute PlaceDocument place)
	{
		place.setPlaceId(UUID.randomUUID());
		pr.save(place);
		return "placeViews/success";
	}
	
	
}
