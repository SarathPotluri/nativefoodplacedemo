package com.nativeFood.documents;

import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Place")
public class PlaceDocument {
	
	@Id
	private UUID placeId;
	private String place;
	private String state;
	private String country;
	private Integer pincode;
	public UUID getPlaceId() {
		return placeId;
	}
	public void setPlaceId(UUID placeId) {
		this.placeId = placeId;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getPincode() {
		return pincode;
	}
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
		
	
}
