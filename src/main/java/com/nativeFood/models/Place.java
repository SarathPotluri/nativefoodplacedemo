package com.nativeFood.models;

import java.util.UUID;

public class Place {
	
    private UUID placeId;
	private String place;
	private String state;
	private String country;
	private String pincode;
	public UUID getPlaceId() {
		return placeId;
	}
	public void setPlaceId(UUID placeId) {
		this.placeId = placeId;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	

}
