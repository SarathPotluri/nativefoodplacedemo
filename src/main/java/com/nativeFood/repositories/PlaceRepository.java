package com.nativeFood.repositories;



import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.nativeFood.documents.PlaceDocument;


@Repository("pr")
public interface PlaceRepository extends MongoRepository<PlaceDocument, UUID>
{
	PlaceDocument findByPlaceId(UUID placeId);
}
