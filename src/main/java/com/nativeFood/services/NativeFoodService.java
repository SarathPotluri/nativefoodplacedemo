package com.nativeFood.services;

import java.util.List;
import java.util.UUID;

import com.nativeFood.documents.PlaceDocument;

public interface NativeFoodService {
	PlaceDocument getPlaceByPlaceId(UUID data);
	List<PlaceDocument> getlist();
	 PlaceDocument createPlace(PlaceDocument pd);
	 /*String update(String place,PlaceDocument pd);
	 String delete(String id);*/
	String update(UUID data, PlaceDocument pd);
	String delete(UUID data);
	String deleteTotal();
	
	

}
