package com.nativeFood.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nativeFood.documents.PlaceDocument;
import com.nativeFood.repositories.PlaceRepository;

@Service("nsi")
public class NativeFoodServiceImpl implements NativeFoodService {

	@Autowired
	PlaceRepository pr;
	@Override
	public List<PlaceDocument> getlist() {
		System.out.println("get Total List");
		return pr.findAll();
	}

	@Override
	public PlaceDocument createPlace(PlaceDocument pd) {
		System.out.println("To create new place");
		return pr.insert(pd);
	}

	/*@Override
	public String update(String place,PlaceDocument pd) 
	{
	PlaceDocument pd1=pr.findByPlace(place);
	if(place=="")
	{
		return "place is not found";
	}
	pd1.setPlace(pd.getPlace());
	pd1.setState(pd.getState());
	pd1.setCountry(pd.getCountry());
	pd1.setPincode(pd1.getPincode());
	pr.save(pd1);
	return "Place is updated";
	}*/
/*
	@Override
	public String delete(UUID id) {
		pr.deleteById(id);
		return "Place Has To be deleted";
	}
*/
	@Override
	public PlaceDocument getPlaceByPlaceId(UUID data) {
     System.out.println("Get Perticular Place Details");
	return pr.findByPlaceId(data);
	
	}

	@Override
	public String update(UUID data, PlaceDocument pd) {
		PlaceDocument pd1=pr.findByPlaceId(data);
		
		pd1.setPlace(pd.getPlace());
		pd1.setState(pd.getState());
		pd1.setCountry(pd.getCountry());
		pd1.setPincode(pd1.getPincode());
		pr.save(pd1);
		return "Place is updated";
		
	
	}

	@Override
	public String delete(UUID data) {
		pr.deleteById(data);
		return "DOcument deleted successfully";
	}

	@Override
	public String deleteTotal() {
		pr.deleteAll();
		return "All Documents are Deleted";
	}

}
